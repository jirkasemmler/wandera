const add = function (x, y) {
    return x + y
};
const multiply = function (x, y) {
    return x * y
};

const transformedAdd = transform(add);
// small testing
console.log(transformedAdd(2)(3));  // 5
console.log(add(2, 3));  // 5

const transformedMult = transform(multiply);
console.log(transformedMult(2)(3)); // 6
console.log(multiply(2, 3));  // 6

function transform(fun) {
    // can be used _.curry, but for binary functions this one is enough.
    // But practically we are talking about curryfication
    return (a) => (b) => fun(a, b);
}
