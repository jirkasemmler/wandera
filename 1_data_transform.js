// not really needed, since Chrome70 supports flatten, but my node doesn't :)
var _ = require('lodash');
var fs = require('fs');
var input = JSON.parse(fs.readFileSync('input.json', 'utf8'));
var output = JSON.parse(fs.readFileSync('output.json', 'utf8'));

var transformed = transformData(input);

// naive testing
if (JSON.stringify(transformed) === JSON.stringify(output)) {
    console.log("ok");
    console.log(transformed);
    console.log(output);
}
else {
    console.log("fail");
    console.log(transformed);
    console.log(output);
}

function transformData(inputJsonData) {
    // flatten to extract nodes from deeper array
    // values to get values from object (aka obj -> array)
    return _.flatten(
        _.values(inputJsonData).map((categoryItem) => // iterate over each category
            // get nodes of category which are checked. This returns nodes per category -> need flatten
            categoryItem.nodes.filter((item) => item.checked)
        )
    )
    // transform node to needed output format
        .map((item) => {
            return {name: item.label}
        })
        // sorting by alphabet
        .sort((a, b) => a.name > b.name);
}
