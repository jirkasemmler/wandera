# javascript-homework

## 1. Data Structure Transformation Assignment

Implement a transformation function that takes data in the format of `input.json` and returns data in the format of `output.json`.

### Input data
Represent a traffic classification rules with some rules selected to be blocked (indicated by `checked: true`). Rules are grouped into categories.

### Output data
Are a simplified set of only rules to be blocked, without any extra attributes, sorted by name.

### Transformation
* solution should be single function taking input format and producing output format
* get rid of categorization
* rename `label` attribute into `name`
* use functional approach
    * no state mutation
    * no `for` loops
    * ideally no intermediate variables
    * therefore ideally some functional composition
* solution should use plain ES6 and optionally one js library of choice
* code will be evaluated in Chrome 70 (no need to polyfill for other/older browsers)


## 2. Manipulating Functions Assignment

Write a generic function that transforms binary functions into composition of unary functions.
Example of binary functions:
```
const add = (x, y) => x + y
const multiply = (x, y) => x * y
```
We are looking for a function `transform` that can do this:
```
const trandformedAdd = transform(add)
transformedAdd(2)(3) == add(2,  3)
```
# Solution

## 1. Data transformation
Solved in 1_data_transform.js. Needs lodash and access to fs because of loading data from fs for testing.
Tested with node

## 2. Curry
Solved in 2_curry.js.
Tested with node
